/**
 * Given an array of positive and negative integers, this function will find the closest number to zero.
 * If the closest number in input could be either negative or positive, the function returns the positive one.
 *
 * @param {Array<Number>} numbers
 * @returns {Number}
 */
module.exports = function closestToZero(numbers) {
  if (
    typeof numbers === 'undefined' ||
    !Array.isArray(numbers) ||
    numbers.length <= 0
  ) {
    return 0;
  }

  // we only need to iterate once on the given input so the run-time complexity is O(n)
  return numbers.reduce((closestInt, currentInt) => {
    const absoluteGap = Math.abs(closestInt) - Math.abs(currentInt);

    // if the gap is positive, it means that the current integer is lower
    // than the previously found.
    // If there's no gap, then the number is of same value but (potentially) different sign
    // so we only return the positive one
    if (absoluteGap > 0) {
      return currentInt;
    } else if (absoluteGap === 0) {
      const isCurrentPositive = Math.sign(currentInt) === 1;
      return isCurrentPositive ? currentInt : closestInt;
    }

    return closestInt;
  });
};
