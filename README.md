# gojob-pretest

Given an array of positive and negative integers, a function returns the closest number to zero. If the closest number in input could be either negative or positive, the function returns the positive one.