const closestToZero = require('./index');

test('retrieve the closest-to-zero number against a list of integers', () => {
  const expectedResults = new Map([
    [[8, 5, 10], 5],
    [[5, 4, -9, 6, -10, -1, 8], -1]
  ]);

  for (const [numbers, expectedRes] of expectedResults) {
    const res = closestToZero(numbers);
    expect(res).toBe(expectedRes);
  }
});

test('non-integers should also work with the function', () => {
  const expectedResults = new Map([
    [[-1 / 2, 5 / 3, 0.5], 0.5],
    [[1337, 42, NaN], 42]
  ]);

  for (const [numbers, expectedRes] of expectedResults) {
    const res = closestToZero(numbers);
    expect(res).toBe(expectedRes);
  }
});

test('unexpected values must always return zero', () => {
  const expectedResults = new Map([
    [undefined, 0],
    ['[]', 0],
    [[], 0]
  ]);

  for (const [numbers, expectedRes] of expectedResults) {
    const res = closestToZero(numbers);
    expect(res).toBe(expectedRes);
  }
});

test('always return the positive integer in case both signs were found', () => {
  const numbers = [8, 2, 3, -2];
  const expectedRes = 2;
  const res = closestToZero(numbers);

  expect(res).toBe(expectedRes);
});
